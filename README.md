# Toy Robot Simulator -- Ruby

A CLI simulator of a toy robot, moving around a table.

## Table of contents:

* [Description](./README.md#description)
  * [Constraints](./README.md#constraints)
  * [Example Input and Output](./README.md#example-input-and-output)
* [Setup](./README.md#setup)
* [Running the app](./README.md#running-the-app)
* [Running the tests](./README.md#running-the-tests)

## Description

* The application is a simulation of a toy robot moving on a square tabletop, of dimensions 6 units x 6 units.

* There are no other obstructions on the table surface.

* The robot is free to roam around the surface of the table, but must be prevented from falling to destruction. Any movement that would result in the robot falling from the table must be prevented, however further valid movement commands must still be allowed.

* The game also supports multiple robots, each robot cannot collide each other

Create an application that can read in commands of the following form:

```
NAME: PLACE X,Y,F
NAME: MOVE
NAME: LEFT
NAME: RIGHT
NAME: REPORT
```

* `NAME` is named by robot

* `PLACE` will put the toy robot on the table in position X,Y and facing NORTH, SOUTH, EAST or WEST.

* The origin (0,0) can be considered to be the SOUTH WEST most corner.

* The first valid command to the robot is a `PLACE` command, after that, any sequence of commands may be issued, in any order, including another `PLACE` command. The application should discard all commands in the sequence until a valid `PLACE` command has been executed

* `MOVE` will move the toy robot one unit forward in the direction it is currently facing.

* `LEFT` and `RIGHT` will rotate the robot 90 degrees in the specified direction without changing the position of the robot.

* `REPORT` will announce the X,Y and F of the robot. This can be in any form, but standard output is sufficient.

* Input can be from a file, or from standard input, as the developer chooses.

### Constraints

* The toy robot must not fall off the table during movement. This also includes the initial placement of the toy robot.

* Any move that would cause the robot to fall must be ignored.

* Any robot cannot collide each other

### Example Input and Output:

#### Example a

    ALICE: PLACE 0,0,NORTH
    ALICE: MOVE
    ALICE: REPORT

Expected output:

    ALICE: 0,1,NORTH

#### Example b

    BOB: PLACE 0,0,NORTH
    BOB: LEFT
    BOB: REPORT

Expected output:

    BOB: 0,0,WEST

#### Example c

    BOB: PLACE 1,3,SOUTH
    ALICE: PLACE 0,1,EAST
    ALICE: MOVE
    BOB: MOVE
    BOB: MOVE
    ALICE: MOVE
    BOB: MOVE
    BOB: LEFT
    ALICE: REPORT
    BOB: REPORT

Expected output

    ALICE: 2,1,EAST
    BOB: 1,1,EAST
    

## Setup

1. Make sure you have Ruby 2.5.1 installed in your machine. If you need help installing Ruby, take a look at the [official installation guide](https://www.ruby-lang.org/en/documentation/installation/).

2. Install the [bundler gem](http://bundler.io/) by running:

    ```gem install bundler```

3. Clone this repo:

    ```git clone git@github.com:kingsleywang2013/toy_robot_simulator.git```

4. Change to the app directory:

    ```cd toy_robot_simulator```

5. Install dependencies:

    ```bundle install```

And you're ready to go!

### Running the app:
```ruby main.rb```

### Running the app with file
```ruby main.rb -f test_success.txt```
or
```ruby main.rb --file test_success.txt```

```ruby main.rb -f test_robots_collide.txt```
or
```ruby main.rb --file test_robots_collide.txt```

```ruby main.rb -f test_robot_hit_edge.txt```
or
```ruby main.rb --file test_robot_hit_edge.txt```

### Running the tests:
```bundle exec rspec```
