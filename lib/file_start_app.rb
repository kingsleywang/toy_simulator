class FileStartApp
  def initialize(file, simulator)
    @file = file
    @simulator = simulator
  end

  def run
    begin
      robot_commands = File.readlines(@file)
    rescue StandardError => e
      puts e
      return
    end

    robot_commands.each do |command|
      begin
        output = @simulator.run(command)
      rescue StandardError => e
        puts e
        return
      end
      puts output if output
    end
  end
end
