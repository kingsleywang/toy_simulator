class Robot

  # Allow instance of 'Robot' object to get and set the following attributes at outside of 'Robot' class
  attr_accessor :pos_x, :pos_y, :direction

  # Can get robot's name only
  attr_reader :name

  # Define a constant for directions for robot
  DIRECTIONS = [:NORTH, :EAST, :SOUTH, :WEST].freeze

  # Create a robot with 0,0 position and facing North at default
  def initialize(name, x = nil, y = nil, direction = nil)
    @name = name
    @pos_x = x
    @pos_y = y
    @direction = direction
  end

  def self.find_or_create_robot(robots, name)
    robot = robots.select { |robot| robot.name == name }.first
    if robot.nil?
      Robot.new(name)
    else
      robot
    end
  end

  # Set robot's current position
  def set_position(x, y)
    @pos_x = x
    @pos_y = y
  end

  # Set robot's direction by checking if the direction is valid
  def set_direction(direction)
    RobotException.invalid_direction_exception(direction) unless DIRECTIONS.include? direction

    @direction = direction
  end

  # Allow robot to turn left and change it's direction
  def rotate_left
    # Get current robot's direction
    index = DIRECTIONS.index(@direction)
    @direction = index.zero? ? DIRECTIONS[-1] : DIRECTIONS[index - 1]
  end

  # Allow robot to turn right and change it's direction
  def rotate_right
    index = DIRECTIONS.index(@direction)
    @direction = index == DIRECTIONS.length - 1 ? DIRECTIONS[0] : DIRECTIONS[index + 1]
  end
end
