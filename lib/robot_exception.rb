class RobotException < StandardError
  class << self
    def action_commands_exception
      raise RobotException, "Command should follow the format 'Name: Actions'"
    end

    def func_commands_exception(func)
      raise RobotException, func.to_s + ' is not a valid commmand'
    end

    def robot_not_place_exception(name)
      raise RobotException, "Robot #{name} is still not placed on the table, pleae `PLACE` first"
    end

    def action_arguments_exception(func)
      raise RobotException, "#{func} should not accept any other arguments"
    end

    def place_arguments_exception
      raise RobotException, 'PLACE command should accept args follow the format like `1,1,NORTH`'
    end

    def position_exception
      raise RobotException, 'Position must be Integer'
    end

    def direction_exception(direction)
      raise RobotException, "#{direction} not includes in #{Robot::DIRECTIONS}"
    end

    def out_boundary_exception
      raise RobotException, 'The move is out of boundary'
    end

    def robots_collide_exception(x, y)
      raise RobotException, "There is another robot in this position #(x:#{x}, y:#{y})"
    end

    def invalid_direction_exception(direction)
      raise RobotException, direction.to_s + ' is an invalid direction'
    end
  end
end