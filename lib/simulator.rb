class Simulator

  # Define a constant for all valid commands for simulator
  COMMANDS = [:PLACE,:MOVE,:LEFT,:RIGHT,:REPORT,:HELP].freeze

  # Create a simulator with initializing table and robot objects
  def initialize
    @table = Table.new
    @robots = []
    @utility = Utility.new
  end

  # Create execution method for simulator
  def run(command)
    # Split user input command to three parts
    # name is robot name
    # func is a command action such as 'PLACE', 'MOVE', 'LEFT', 'RIGHT', 'REPORT', 'HELP'
    # args is a set of position and direction such as '1,2,NORTH'
    (name, actions) = @utility.extract_commands(command)

    @utility.check_name_actions(name, actions)

    # Get func and args if command is not single word and equal to 'HELP'
    (func, args) = @utility.extract_func_arg_commands(actions) unless name == 'HELP'

    @utility.check_func_args(func, args)

    robot = Robot.find_or_create_robot(@robots, name.strip)
    simulator_func(name, func.to_sym, args, robot) unless func.nil?
  end

  private

  def simulator_func(name, func, args, robot)
    # Execute different simulator's methods according to command
    if func == :PLACE
      cmd_place robot, args
      return
    else
      @utility.robot_placed?(name, robot)
      run_robot_func(func, robot)
    end
  end

  # Method for placing a robot in a table according to position and direction from the user input command
  def cmd_place(robot, args)
    (x, y, direction) = args.split(',')
    @table.place_robot(x.to_i, y.to_i, direction.to_sym, robot, @robots)
    @robots << robot
    @robots = @robots.uniq(&:name)
  end

  # Method for moving robot forwards
  def cmd_move(robot)
    @table.move_robot_forwards robot
  end

  # Method for robot to turn left
  def cmd_left(robot)
    @table.rotate_robot_left robot
  end

  # Method for robot to turn right
  def cmd_right(robot)
    @table.rotate_robot_right robot
  end

  # Method for robot to report its current position and direction
  def cmd_report(robot)
    robot.name + ': ' + @table.get_robot_position(robot) + ',' + @table.get_robot_direction(robot)
  end

  def run_robot_func(func, robot)
    case func
    when :MOVE
      cmd_move(robot)
      return
    when :LEFT
      cmd_left(robot)
      return
    when :RIGHT
      cmd_right(robot)
      return
    when :REPORT
      cmd_report(robot)
    end
  end
end
