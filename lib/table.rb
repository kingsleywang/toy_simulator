class Table
  # Create a table with 6 x 6 units when initialize
  def initialize(width = 5, height = 5)
    @width = width
    @height = height
  end

  # Initialize robot object in the 'Table' class
  def place_robot(x, y, direction, robot, robots)
    @robots = robots
    set_position(x, y, direction, robot)
  end

  # Set robot's current position through 'Table' object
  def set_position(x, y, direction, robot)
    # Check if the position is valid (inside of table 6 x 6 units)
    RobotException.out_boundary_exception if x > @width || y > @height || x < 0 || y < 0

    @robots.each do |robot|
      RobotException.robots_collide_exception(x, y) if x == robot.pos_x && y == robot.pos_y
    end

    robot.set_position(x, y)
    robot.set_direction(direction)
  end

  # Get current robot's position
  def get_robot_position(robot)
    robot.pos_x.to_s + ',' + robot.pos_y.to_s
  end

  # Get current robot's direction
  def get_robot_direction(robot)
    robot.direction.to_s
  end

  # Command robot turn left through 'Table' object
  def rotate_robot_left(robot)
    robot.rotate_left
  end

  # Command robot turn right through 'Table' object
  def rotate_robot_right(robot)
    robot.rotate_right
  end

  # Command robot move forwards according to it's direction through 'Table' object
  def move_robot_forwards(robot)
    case robot.direction
    when :NORTH
      set_position(robot.pos_x, robot.pos_y + 1, :NORTH, robot)
    when :EAST
      set_position(robot.pos_x + 1, robot.pos_y, :EAST, robot)
    when :SOUTH
      set_position(robot.pos_x, robot.pos_y - 1, :SOUTH, robot)
    when :WEST
      set_position(robot.pos_x - 1, robot.pos_y, :WEST, robot)
    end
  end
end
