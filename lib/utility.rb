class Utility
  def extract_commands(command)
    command.upcase.split(':', 2)
  end

  def extract_func_arg_commands(actions)
    actions.strip.upcase.split(' ', 2)
  end

  def check_name_actions(name, actions)
    RobotException.action_commands_exception if (actions.nil? || actions == '') && name != 'HELP'
  end

  def check_func_args(func, args)
    # Check if the input command is valid in our simulator default commands
    RobotException.func_commands_exception(func) if !func.nil? && !Simulator::COMMANDS.include?(func.to_sym)

    # Check if func and args combo is valid
    check_correct_action(func, args)
  end

  def robot_placed?(name, robot)
    RobotException.robot_not_place_exception(name) if robot.direction.nil?
  end

  private

  def is_integer?(n)
    !!(n.match /^(\d)+$/)
  end

  def check_correct_action(func, args)
    RobotException.action_arguments_exception(func) if Simulator::COMMANDS[1..-1].include?(func.to_sym) && !args.nil?
    check_func_is_place(args) if func.to_sym == :PLACE
  end

  def check_func_is_place(args)
    RobotException.place_arguments_exception if args.nil? || args.split(',').size != 3
    (x, y, direction) = args.split(',')
    RobotException.position_exception if !is_integer?(x.strip) || !is_integer?(y.strip)
    RobotException.direction_exception(direction) unless Robot::DIRECTIONS.include?(direction.to_sym)
  end
end