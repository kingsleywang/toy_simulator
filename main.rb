require_relative './lib/robot'
require_relative './lib/simulator'
require_relative './lib/table'
require_relative './lib/file_start_app'
require_relative './lib/input_start_app'
require_relative './lib/robot_exception'
require_relative './lib/utility'

simulator = Simulator.new

option, file = ARGV
if (option == '-f' || option == '--file') && !file.nil?
  FileStartApp.new(file, simulator).run
else
  InputStartApp.new(simulator).run
end
