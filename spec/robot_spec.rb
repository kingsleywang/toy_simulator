require 'spec_helper'

RSpec.describe 'Robot' do
  before do
    @robot = Robot.new('TEST', 1, 1, :NORTH)
  end

  context 'find_or_create_robot' do
    it 'return exist robot' do
      expect(Robot.find_or_create_robot([@robot], 'TEST')).to eq @robot
    end

    it 'create a new robot' do
      new_robot = Robot.find_or_create_robot([], 'ABC')
      expect(new_robot).not_to eq @robot
      expect(new_robot.name).to eq 'ABC'
    end
  end

  it 'should return valid direction' do
    @robot.set_direction(:WEST)
    expect(@robot.direction).to eq(:WEST)
  end

  it 'should return error with invalid direction' do
    expect { @robot.set_direction(:HAHA) }.to raise_error(RobotException, 'HAHA is an invalid direction')
  end

  it 'should rotate left' do
    @robot.rotate_left
    expect(@robot.direction).to eq(:WEST)
  end

  it 'should rotate right' do
    @robot.rotate_right
    expect(@robot.direction).to eq(:EAST)
  end
end
