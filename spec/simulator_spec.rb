require 'spec_helper'

RSpec.describe 'Simulator' do

  before do
    @simulator = Simulator.new
  end

  context 'invalid commands' do
    it 'command format incorrect' do
      expect { @simulator.run('TEST') }.to raise_error(RobotException, "Command should follow the format 'Name: Actions'")
    end

    it 'command format incorrect with only colon' do
      expect { @simulator.run('TEST:') }.to raise_error(RobotException, "Command should follow the format 'Name: Actions'")
    end

    it 'command not found' do
      expect { @simulator.run('TEST: TEST') }.to raise_error(RobotException, 'TEST is not a valid commmand')
    end

    it 'no any robot place' do
      expect { @simulator.run('TEST: MOVE') }.to raise_error(RobotException, 'Robot TEST is still not placed on the table, pleae `PLACE` first')
    end

    it 'wrong place command' do
      expect { @simulator.run('TEST: PLACE 1,2') }.to raise_error(RobotException, 'PLACE command should accept args follow the format like `1,1,NORTH`')
    end

    it 'wrong action command after PLACE action' do
      @simulator.run('TEST: PLACE 1,2,NORTH')
      expect { @simulator.run('TEST: MOVE TEST') }.to raise_error(RobotException, 'MOVE should not accept any other arguments')
    end
  end

  context 'valid commands' do
    describe 'normal actions' do
      it 'place move command' do
        @simulator.run('TEST: PLACE 0,0,NORTH')
        @simulator.run('TEST: MOVE')
        expect(@simulator.run('TEST: REPORT')).to  eq('TEST: 0,1,NORTH')
      end

      it 'place left command' do
        @simulator.run('TEST: PLACE 0,0,NORTH')
        @simulator.run('TEST: LEFT')
        expect(@simulator.run('TEST: REPORT')).to eq('TEST: 0,0,WEST')
      end

      it 'place move move left move command' do
        @simulator.run('TEST: PLACE 1,2,EAST')
        @simulator.run('TEST: MOVE')
        @simulator.run('TEST: MOVE')
        @simulator.run('TEST: LEFT')
        @simulator.run('TEST: MOVE')
        expect(@simulator.run('TEST: REPORT')).to eq('TEST: 3,3,NORTH')
      end

      it 'place move move right move command' do
        @simulator.run('TEST: PLACE 1,2,EAST')
        @simulator.run('TEST: MOVE')
        @simulator.run('TEST: MOVE')
        @simulator.run('TEST: RIGHT')
        @simulator.run('TEST: MOVE')
        expect(@simulator.run('TEST: REPORT')).to eq('TEST: 3,1,SOUTH')
      end

      it 'case insensitive' do
        @simulator.run('test: place 0,0,north')
        @simulator.run('test: left')
        expect(@simulator.run('test: REPORT')).to eq('TEST: 0,0,WEST')
      end
    end

    describe 'multiple robots' do
      it 'get correct report' do
        @simulator.run('TEST1: PLACE 1,2,EAST')
        @simulator.run('TEST2: PLACE 2,2,EAST')
        expect(@simulator.run('TEST1: REPORT')).to eq('TEST1: 1,2,EAST')
        expect(@simulator.run('TEST2: REPORT')).to eq('TEST2: 2,2,EAST')
      end

      it 'place table with collide' do
        @simulator.run('TEST1: PLACE 1,2,EAST')
        expect { @simulator.run('TEST2: PLACE 1,2,NORTH') }.to raise_error(RobotException, 'There is another robot in this position #(x:1, y:2)')
      end

      it 'move and meet collide' do
        @simulator.run('TEST1: PLACE 1,2,EAST')
        @simulator.run('TEST2: PLACE 2,2,EAST')
        expect { @simulator.run('TEST1: MOVE') }.to raise_error(RobotException, 'There is another robot in this position #(x:2, y:2)')
      end
    end
  end
end
