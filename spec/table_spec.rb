require 'spec_helper'

RSpec.describe 'Table' do
  before do
    @table = Table.new
    @robot = Robot.new('TEST')
    @table.place_robot(1, 1, :NORTH, @robot, [@robot])
  end

  it 'should place robot in the table' do
    expect(@robot.direction).to eq(:NORTH)
  end

  it 'should get robot position' do
    @table.get_robot_position(@robot)
    expect(@robot.pos_x).to eq(1)
    expect(@robot.pos_y).to eq(1)
  end

  it 'should set position with valid data' do
    @table.set_position(1,2, :EAST, @robot)
    expect(@robot.pos_x).to eq(1)
    expect(@robot.pos_y).to eq(2)
    expect(@robot.direction).to eq(:EAST)
  end

  it 'should return error with invalid width position' do
    expect { @table.set_position(6,2, :WEST, @robot) }.to raise_error(RobotException, 'The move is out of boundary')
  end

  it 'should return error with invalid height position' do
    expect { @table.set_position(3,6, :SOUTH, @robot) }.to raise_error(RobotException, 'The move is out of boundary')
  end

  it 'should get robot direction' do
    expect(@table.get_robot_direction(@robot)).to eq('NORTH')
  end

  it 'should robot rotate to left' do
    @table.rotate_robot_left(@robot)
    expect(@robot.direction).to eq(:WEST)
  end

  it 'should robot rotate to right' do
    @table.rotate_robot_right(@robot)
    expect(@robot.direction).to eq(:EAST)
  end

  it 'should move to next step if facing NORTH' do
    @table.set_position(3,3, :NORTH, @robot)
    @table.move_robot_forwards(@robot)
    expect(@robot.pos_x).to eq 3
    expect(@robot.pos_y).to eq 4
    expect(@robot.direction).to eq(:NORTH)
  end

  it 'should move to next step if facing EAST' do
    @table.set_position(3,3, :EAST, @robot)
    @table.move_robot_forwards(@robot)
    expect(@robot.pos_x).to eq 4
    expect(@robot.pos_y).to eq 3
    expect(@robot.direction).to eq(:EAST)
  end

  it 'should move to next step if facing SOUTH' do
    @table.set_position(3,3, :SOUTH, @robot)
    @table.move_robot_forwards(@robot)
    expect(@robot.pos_x).to eq 3
    expect(@robot.pos_y).to eq 2
    expect(@robot.direction).to eq(:SOUTH)
  end

  it 'should move to next step if facing WEST' do
    @table.set_position(3,3, :WEST, @robot)
    @table.move_robot_forwards(@robot)
    expect(@robot.pos_x).to eq 2
    expect(@robot.pos_y).to eq 3
    expect(@robot.direction).to eq(:WEST)
  end

end
